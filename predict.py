import time
import keras
import keras_resnet.models
import tensorflow as tf
import numpy as np
import cv2
import matplotlib.pyplot as plt
from keras_retinanet.models import load_model
from keras_retinanet import models
from keras_retinanet.utils.image import read_image_bgr, preprocess_image, resize_image
from keras_retinanet.utils.visualization import draw_box, draw_caption
from keras_retinanet.utils.colors import label_color

labels_to_names = {0: 'Logo'}

image = read_image_bgr('/home/cdelcacho/keras-retinanet/paradigma/images/Cartas (1).jpg')
draw = image.copy()
draw = cv2.cvtColor(draw, cv2.COLOR_BGR2RGB)

model = load_model('snapshots/resnet50_csv_50.h5', backbone_name='resnet50')
model = models.convert_model(model)

# preprocess image for network
image = preprocess_image(image)
image, scale = resize_image(image)

# process image
start = time.time()
labels = []
boxes, scores, labels = model.predict_on_batch(np.expand_dims(image, axis=0))

print("processing time: ", time.time() - start)

# correct for image scale
boxes /= scale

# visualize detections
for box, score, label in zip(boxes[0], scores[0], labels[0]):
    # scores are sorted so we can break
    if score < 0.5:
        break

    color = label_color(label)

    b = box.astype(int)
    draw_box(draw, b, color=color)

    caption = "{} {:.3f}".format(labels_to_names[label], score)
    draw_caption(draw, b, caption)

plt.figure(figsize=(15, 15))
plt.axis('off')
plt.imshow(draw)
plt.show()
