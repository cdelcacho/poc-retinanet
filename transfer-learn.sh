python3 keras_retinanet/bin/train.py --steps 100 --epochs 50 --weights resnet50_coco_best_v2.1.0.h5 --freeze-backbone --random-transform csv dataset/train.csv dataset/classes.csv
